// Wait for window load
$(window).load(function() {
/*      if (window.location.href.indexOf('reload')==-1) {
           window.location.replace(window.location.href+'?reload');
      }*/
  // Animate loader off screen
  $(".se-pre-con").fadeOut("slow");
});

$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});



$(".nav-on").click(function(){
    $(".navbar").removeClass("min")
	$(".nav-off").removeClass("hidden")
	$(".nav-on").addClass("hidden");
});

$(".nav-off").click(function(){
    $(".navbar").addClass("min")
	$(".nav-off").addClass("hidden")
	$(".nav-on").removeClass("hidden");
});


$("#moveSectionDown").hide();
var i = null;
$(".section").mousemove(function() {
    clearTimeout(i);
    $("#moveSectionDown").show();
    i = setTimeout('$("#moveSectionDown").hide();', 1000);
}).mouseleave(function() {
    clearTimeout(i);
    $("#moveSectionDown").hide();  
});

$("#moveSectionDown").hide();
var i = null;
$(".scroll-bt").mousemove(function() {
    clearTimeout(i);
    $("#moveSectionDown").show();
    i = setTimeout('$("#moveSectionDown").hide();', 1000);
}).mouseleave(function() {
    clearTimeout(i);
    $("#moveSectionDown").hide();  
});


$(".sh-btn").click(function(){
    $(".sc-ul").toggleClass("hidden")
	$(this).toggleClass("sh-btn-open");
});


$("#mute-btn").click(function(){
    $(".mute-icon").removeClass("fa-volume-up")
	$(".mute-icon").addClass("fa-volume-off");
});




window.my_mute = false;
$("#mute-btn").click(function(){
 
var get_status = $(".mute-icon").attr("data-status");
if(get_status =='fa-volume-up')
{
    $(".mute-icon").removeClass("fa-volume-up")
 $(".mute-icon").addClass("fa-volume-off");
 $(".mute-icon").attr("data-status","fa-volume-off");
 $('video').each(function(){
	 $(this).prop('muted', true);
    });
}
else
{
    $(".mute-icon").removeClass("fa-volume-off")
 $(".mute-icon").addClass("fa-volume-up");
 $(".mute-icon").attr("data-status","fa-volume-up");
 $('video').each(function(){
	 $(this).prop('muted', false);
    });
}
});








